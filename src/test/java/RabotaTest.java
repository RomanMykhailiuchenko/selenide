import com.codeborne.selenide.Driver;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.testng.Assert.assertEquals;

public class RabotaTest {
    @Test(priority = 1)
    public void firstTest(){
        open("https://rabota.ua");
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/div/ul/li[4]/a[1]/label")).click();
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/a/img[4]")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_txbLogin\"]")).shouldBe(visible);
        $(By.cssSelector("#ctl00_Sidebar_login_txbPassword")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_pnlLoginForm\"]/div/div/div[1]/h3")).shouldHave(text("Вход в аккаунт"));
        $("#ctl00_Sidebar_login_lnkLogin").shouldBe(visible);
        screenshot("Test_1");
    }
    @Test(enabled = false, description = "critical checks")
    public void secondTest() {

        open("https://www.rabota.ua/");
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/div/ul/li[4]/a[1]/label")).click();
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_txbLogin\"]")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_txbPassword\"]")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Sidebar_login_lnkLogin\"]")).shouldBe(visible);
        screenshot("Test_2");
    }

    @Test(priority = 2, description = "Sanity")
    public void thirdTest() {
        open("https://www.rabota.ua/");
        $(By.xpath("//*[@id=\"aspnetForm\"]/footer/div[1]/div/div/nav[1]/div/ul/li[1]/span/label")).click();
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/div/ul/li[4]/a[2]/span")).shouldBe(visible);
        $(By.xpath("//*[@id=\"ctl00_Header_header\"]/div/header/div/div/ul/li[4]/a[1]/label")).shouldHave(text("Увійти"));
        screenshot("Test_3");

    }

    @Test(priority = 3, description = "Sanity2")
    public void fourthTest() {
        open("https://www.rabota.ua/ua");
        $(By.xpath("//*[@id=\"ctl00_content_vacSearch_CityPickerWork_inpCity\"]")).setValue("Харьков").pressEnter();
        $(By.xpath("//div/div[3]//div[3]/input")).shouldBe(visible);
        screenshot("Test_4");
    }

    @Test(priority = 4, description = "Sanity3")
    public void fifthTest() {
        open("https://www.rabota.ua/");
        $(By.xpath("//*[contains(@id,\"Search_Keyword\")]")).setValue("QA automation engineer").pressEnter();
        //$(By.xpath("//*[@id=\"7454160\"]/td/article/div[1]/div/h3/a")).waitUntil(exist, 1000);
        $(By.xpath("//*[@id=\"newheader\"]//input")).shouldBe(visible);
        screenshot("Test_5");
    }
    }

